CC ?= gcc
CFLAGS ?= -O2 -g -Wall
LOADLIBES ?= -lXext -lX11 -lXt
LDFLAGS ?= -L/usr/X11R6/lib

prefix?=/usr/local
mandir?=$(prefix)/share/man
bindir?=$(prefix)/bin

INSTALL ?= install
INSTALL_PROGRAM ?= $(INSTALL) --mode a+rx,ug+w
INSTALL_DIR ?= mkdir --parent

# Use this for solaris
#CFLAGS = -O2 -pipe -Wall
#LOADLIBES = -lsocket -lnsl -lresolv -lXext -lX11
#LDFLAGS = -L/usr/openwin/lib

all: xlassie

xlassie: xlassie.o socket.o
xlassie.o: defaults.h
socket.o: defaults.h

xlassie.1x: xlassie xlassie.h2m
	help2man --include xlassie.h2m --section=1x --no-info --output xlassie.1x ./xlassie

dist: clean
	@(dir=`pwd`; name=`basename $$dir`; \
	rm -f $$name.tar.gz ; \
	cd .. ; tar -zcf $$name/$$name.tar.gz --exclude $$name/$$name.tar.gz $$name )

clean:
	-rm -f *.o xlassie xlassie.1x

install: xlassie xlassie.1x
	$(INSTALL_DIR) $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) xlassie $(DESTDIR)$(bindir)/
	$(INSTALL_DIR) $(DESTDIR)$(mandir)/man1
	$(INSTALL) xlassie.1x $(DESTDIR)$(mandir)/man1/

install-strip:
	$(MAKE) INSTALL_PROGRAM='$(INSTALL_PROGRAM) --strip' install

.PHONY: all clean dist install
