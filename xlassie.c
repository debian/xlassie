/* Copyright (C) 1998 Trent Piepho  <xyzzy@u.washington.edu>
 *           (C) 1999-2001 Trent Piepho  <xyzzy@speakeasy.org>
 *   Updates (C) 2004 Barak A. Pearlmutter <barak@cs.nuim.ie>
 *                    Nadim Shaikli <nadim@arabeyes.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc., 675
 * Mass Ave, Cambridge, MA 02139, USA.  */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <time.h>
#include <pwd.h>
#include <stdarg.h>
#include <errno.h>
#include <termios.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <X11/extensions/shape.h>

#include "defaults.h"

#define VERSION         "1.8"
#define APL_CLASS       "XBiff"		/* class name to check as well */
#define APL_SUBCLASS    "xlassie"	/* application class name */

/* X related globals */
Display *dpy;
Window win;
GC gc;
XFontStruct *font = 0;
int Ascent=33,Descent=0;
int Width=55,Height=33;
XColor FgXColor,HiXColor;
Pixmap ShapeMask;			/* shape stuff */
GC ShapeGC;				/* shape stuff */
int MaskWidth,MaskHeight;		/* shape stuff */
Atom DeleteWindow;			/* Atom of delete window message */
Atom KWMDockWindow;			/* Atom for KWM hint */

/* app-default related globals */
char *DisplayName = NULL;
int Options = 0;
int Number;
int Count_offset = 0;

char *BackupFonts[][7] = {
  { FONTNAME, FONTNAME0, FONTNAME1, FONTNAME2, FONTNAME3, FONTNAME4, 0 },
  { KDEFONT, KDEFONT0, KDEFONT1, KDEFONT2, KDEFONT3, KDEFONT4, 0 },
  { WMFONT, WMFONT0, WMFONT1, WMFONT2, WMFONT3, WMFONT4, 0 }};

void usage();
void close_display();
void update_led(int toggle);
void update();
void handler(int);
void parse_cmdline(int argc,char *argv[]);
void init(int argc,char *argv[]);
int count_mail();

/* Local sub-options */
#define VERBOSE         (1<<0)
#define USE_MAILDIR     (1<<1)
#define KDE2_MODE       (1<<2)

/* List out all the various options used */
/* This enumerated list and the array of stuctures 'optList' __MUST__
 * always be in sycn and must have a one-to-one correspondance.
 */
enum optOptions
  {
    NAME,
    BACKGROUND,
    FOREGROUND,
    HIGHLIGHT,
    UPDATE,
    BELL,
    LED,
    FONT,
    GEOMETRY,
    SHAPE,
    SPOOL,
    MAILCOMMAND,
    CLICKCOMMAND,
    POP3,
    APOP3,
    IMAP,
    FOLDERNAME,
    USERNAME,
    PASSWORD,
    OFFLINE,
    WMAKER,
    KDE,
    NOKDE,
    NOWMAKER
  };

typedef struct
{
  const char        *label;         /* label/long name */
  const char        *name;          /* alternate/short name */
  const int         isBool;         /* entry is a boolean function */
  int               isSet;          /* indicate if set */
  char              *value;         /* specified value/setting */
} optStruct;

// This array and 'optOptions' __MUST__ always be in sync
optStruct optList[] =
  {
    {"name", "name",			False,	False, APL_SUBCLASS},
    {"background", "bg",		False,	False, "White"},
    {"foreground", "fg",		False,	False, "Black"},
    {"highlight", "hilight",		False,	False, "Red"},
    {"update", "update",		False,	False, INTERVAL_SPOOL},
    {"bell", "bell",			True,	False, NULL},
    {"led", "led",			False,	False, "1"},
    {"font", "fn",			False,	False, NULL},
    {"geometry", "geometry",		False,	False, NULL},
    {"shape", "shape",			True,	False, NULL},
    {"spool", "spool",			False,	False, ""},
    {"mailcommand", "mailcmd",		False,	False, ""},
    {"clickcommand", "clickcmd",	False,	False, COMMAND},
    {"pop3", "pop3",			False,	False, INTERVAL_SPOOL},
    {"apop3", "apop3",			False,	False, INTERVAL_SPOOL},
    {"imap", "imap",			False,	False, INTERVAL_SPOOL},
    {"imapfolder", "imapfolder",	False,	False, "INBOX"},
    {"username", "username",		False,	False, NULL},
    {"password", "password",		False,	False, ""},
    {"offline", "offline",		True,	False, NULL},
    {"wmaker", "wmaker",		True,	False, NULL},
    {"kde", "kde",			True,	False, NULL},
    {"nokde", "nokde",			True,	False, NULL},
    {"nowmaker", "nowmaker",		True,	False, NULL},
  };


/* ---------- *
 * -* Code *- *
 * ---------- */

void verbose(char *fmt, ...)
{
  va_list ap;
  if (! (Options&VERBOSE)) return;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  fflush(stderr);
}

/*
 * Grab the .Xdefault resources (if any) for this application
 */
void parse_xdefaults(Display *display, const char *app_name)
{
  int	entry;

  for (entry = 0; entry < (sizeof(optList) / sizeof(optList[0])); entry++)
    {
      char		*def1, *def2;
      const char	*e_label	= optList[entry].label;
      const char	*e_name		= optList[entry].name;

      /* previously set somehow - sanity check */
      if (e_label == NULL || optList[entry].isSet != False)
	{
	  verbose("Xdefaults - (%2d) = %s [externally set] = %s",
		  entry, optList[entry].label, optList[entry].value);
	  continue;
	}

      /* Get both long and short name settings */
      def1 = XGetDefault(display, app_name, e_label);
      def2 = XGetDefault(display, app_name, e_name);

      if ((def1 == NULL) && (def2 == NULL))
	{
	  def1 = XGetDefault(display, APL_SUBCLASS, e_label);
	  def2 = XGetDefault(display, APL_SUBCLASS, e_name);
	  if ((def1 == NULL) && (def2 == NULL))
	    {
	      def1 = XGetDefault(display, APL_CLASS, e_label);
	      def2 = XGetDefault(display, APL_CLASS, e_name);
	    }
	}

      /* Short name settings are OK */
      if ((def1 == NULL) && def2)
	def1 = def2;

      /* Set the ultimate value to use */
      if (def1)
	{
	  if (optList[entry].isBool)
	    {
	      if ((strcasecmp(def1, "TRUE") == 0)	||
		  (strcasecmp(def1, "YES") == 0)	||
		  (strcasecmp(def1, "ON") == 0)	||
		  (strcasecmp(def1, "1") == 0))
		{
		  optList[entry].isSet = 1;
		  optList[entry].value = "1";
		}
	      else
		{
		  optList[entry].isSet = 0;
		  optList[entry].value = "0";
		}
	    }
	  else
	    {
	      optList[entry].isSet = 1;
	      optList[entry].value = def1;
	    }
	}
      /* Optionally note what was read from the .Xdefaults file */
      verbose("Xdefaults - (%2d) - %s = %s",
	      entry, optList[entry].label, optList[entry].value);
    }
}

/*
 * Do some clean-up prior to closing display
 */
void close_display()
{
  verbose("Setting LED to OFF");
  update_led(False);
  XCloseDisplay(dpy);
}

/*
 * Toggle specified LED based on availability of email
 * unless instructed to turn it off verbosely
 */
void update_led (int toggle)
{
  XKeyboardControl control;
  verbose("Updating LED: %d", toggle);

  /* At most 32 LEDs numbered from one are supported.  No standard
     interpretation of LEDs is defined or specified anywhere.
  */
  control.led = atoi(optList[LED].value);
  if ((Number > 0) && toggle)
    control.led_mode = LedModeOn;
  else
    control.led_mode = LedModeOff;
  XChangeKeyboardControl(dpy, KBLed | KBLedMode, &control);
}

// Calculate log base 10 of an integer, rounded down
int ilog10(unsigned int x)
{
  int i;
  if (x==0) return -1;
  for (i=0; x>9; x/=10, i+=1)
    ;
  return i;
}

void update()
{
  static int old_number=-1;
  char str[32];
  static int oldw=-1,oldh=-1;
  int w,h;

  verbose("updating window");

  if(optList[OFFLINE].isSet && Number==-1)  {
    strcpy(str,"X");
  } else {
    int number = Number+Count_offset;
    // Since the code doesn't resize the count properly, email
    // counts with more than 99 messages results in weird looking
    // displays.  A short-term solution (this should be fixed by
    // properly resizing the total message count to fit in the
    // area proscribed) is just to give a Full indicator.
    if (number > 99) {
      // Use "e2" for 100-999, ie numbers of the form d.ddde2, etc.
      // We make no provisions for an inbox with over 10^10 messages.
      verbose("count %d exceeds two digits", number);
      sprintf(str,"e%d",ilog10(number));
    } else {
      sprintf(str,"%d",number);
    }
  }

  w = (Width-XTextWidth(font,str,strlen(str)))/2;
  h = (Height+Ascent-Descent)/2;

  if(optList[SHAPE].isSet) {
    if(Number!=old_number || oldw!=w || oldh!=h)  {
      old_number=Number; oldw=w; oldh=h;
      /* these next 3 lines clear the pixmap, is there a cleaner way? */
      XSetFunction(dpy,ShapeGC,GXclear);
      XFillRectangle(dpy,ShapeMask,ShapeGC,0,0,MaskWidth,MaskHeight);
      XSetFunction(dpy,ShapeGC,GXset);
      XDrawString(dpy,ShapeMask,ShapeGC,0,Ascent,str,strlen(str));
      XShapeCombineMask(dpy, win, ShapeBounding,
			w, h-Ascent, ShapeMask, ShapeSet);
    };
  } else {
    XClearWindow(dpy,win);
  }

  if(optList[HIGHLIGHT].isSet) {
    XSetForeground(dpy, gc,
		   (Number+Count_offset) ? HiXColor.pixel : FgXColor.pixel);
  }

  XDrawString(dpy,win,gc,w,h,str,strlen(str));

  if(optList[LED].isSet) update_led(True);
}

void handler(int signum)
{
  int old;

  /* Intercept Ctrl-C's and kill commands */
  if ((signum == SIGINT) || (signum == SIGTERM))
    {
      /* Clean-up and exit */
      verbose("SIGINT handler");
      close_display();
      exit(errno);
    }

  old = Number;
  verbose("SIGALRM handler");
  count_mail();
  verbose("new count %d, old %d", Number, old);
  if(old==Number) {
    return;
  }
  update();
  if(Number>old)  {
    if(optList[BELL].isSet) XBell(dpy,100);
    if(optList[MAILCOMMAND].isSet) {
      char str[256];
      sprintf(str, optList[MAILCOMMAND].value, Number);
      system(str);
    }
  }
  XFlush(dpy);
}

void font_height(void)
{
  int foo,bar,baz;
  XCharStruct extents;

  XTextExtents(font,"0123456789",10,&foo,&bar,&baz,&extents);
  Ascent=extents.ascent;
  Descent=extents.descent;
}

void version(void)
{
  printf("xlassie " VERSION "\n");
}

void usage(void)
{
  version();
  printf("Usage: xlassie [options]\n");
  printf("Options:\n");
  printf("    -h,  -help              Print usage and exit\n");
  printf("    -v,  -version           Print version and exit\n");
  printf("    -V,  -verbose           Chatter on about this and that\n");
  printf("    -name <name>            Set application class name (default: %s)\n", optList[NAME].value);
  printf("    -bg <color>             Background color (default: %s)\n", optList[BACKGROUND].value);
  printf("    -fg <color>             Foreground color (default: %s)\n", optList[FOREGROUND].value);
  printf("    -hilight [<color>]      Use a different foreground color when there\n"
	 "                                is non-zero mail (default: %s)\n", optList[HIGHLIGHT].value);
  printf("    -update <number>        Check mail every <number> seconds\n"
	 "                                (default: %s sec local, %s sec remote)\n", INTERVAL_SPOOL, INTERVAL_REMOTE);
  printf("    -bell                   Ring bell when count increases from zero\n");
  printf("    -led [<number>]         Light-up keyboard LED <number> if mail exists\n");
  printf("                                (default: %d)\n", atoi(optList[LED].value));
  printf("    -fn <fontname>          Font for the new mail count\n");
  printf("    -display <displayname>  X server to connect to\n");
  printf("    -geometry <geometry>    Window geometry\n");
  printf("    -shape                  Use a shaped window\n");
  printf("    -spool <filename>        File to use for mail spool or maildir directory\n");
  printf("    -mailcommand <command>  Command to execute on new mail arrival\n");
  printf("    -clickcommand <command> Command to execute when clicked upon\n");
  printf("                                (default: %s)\n", optList[CLICKCOMMAND].value);
  printf("    -offset <number>        Mail display count offset (for debugging)\n");
#ifdef HAVE_POP3
  printf("    -pop3 <server>          Connect to pop3 server rather than local mail spool\n");
  printf("    -apop3 <server>         Like -pop3, but uses a different method.\n");
  printf("                                Use when -pop3 doesn't find the correct number\n");
  printf("    -imap <server>          Use the IMAP protocol instead of pop3\n");
  printf("    -imapfolder <folder>    Use this folder instead of INBOX\n");
  printf("    -username <name>        Username for pop3/imap server,\n"
	 "                                when different from local username\n");
  printf("    -password <password>    Password to use on pop3/imap server.\n");
  printf("                                Use the password 'ask' to be prompted for\n"
	 "                                the password on stdin.\n");
  printf("    -offline                Don't exit when the server is unreachable\n");
#endif
  printf("    -wmaker, -kde           Do stuff to get swallowed into the WindowMaker\n");
  printf("                                or KDE dock.  Normally unneeded: WindowMaker\n"
	 "                                and KDE should be autodetected.\n");
  printf("    -nokde, -nowmaker       Don't do the stuff to get swallowed\n");
}

void ask_password()
{
  size_t i = 4;
  struct termios saved;
  int got = tcgetattr(0, &saved);
  if (got != -1) {
    struct termios noecho = saved;
    noecho.c_lflag &= ~ECHO;
    tcsetattr(0, TCSADRAIN, &noecho);
  }

  printf("Mail account password: ");
  fflush(stdout);
  i = getline(&optList[PASSWORD].value, &i, stdin);
  if(optList[PASSWORD].value[i - 1] == '\n') {
    putchar('\n');
    optList[PASSWORD].value[i - 1] = '\0';
  }

  if (got != -1)
    tcsetattr(0, TCSADRAIN, &saved);
}

void parse_cmdline(int argc, char *argv[])
{
  while (1) {
    static struct option long_options[] = {
      {"help",			no_argument,		NULL, 'h'},
      {"version",		no_argument,		NULL, 'v'},
      {"verbose",		no_argument,		NULL, 'V'},
      {"name",			required_argument,	NULL, 1},
      {"bg",			required_argument,	NULL, 2},
      {"fg",			required_argument,	NULL, 3},
      {"hilight",		optional_argument,	NULL, 4},
      {"update",		required_argument,	NULL, 5},
      {"bell",			no_argument,		NULL, 6},
      {"led",			optional_argument,	NULL, 7},
      {"fn",			required_argument,	NULL, 8},
      {"font",			required_argument,	NULL, 8},
      {"display",		required_argument,	NULL, 9},
      {"geometry",		required_argument,	NULL, 10},
      {"shape",			no_argument,		NULL, 11},
      {"spool",			required_argument,	NULL, 12},
      {"mailcommand",		required_argument,	NULL, 13},
      {"clickcommand",		required_argument,	NULL, 14},
      {"offset",		required_argument,	NULL, 15},
#ifdef HAVE_POP3
      {"pop3",			required_argument,	NULL, 16},
      {"apop3",			required_argument,	NULL, 17},
      {"imap",			required_argument,	NULL, 18},
      {"imapfolder",		required_argument,	NULL, 19},
      {"username",		required_argument,	NULL, 20},
      {"password",		required_argument,	NULL, 21},
      {"offline",		no_argument,		NULL, 22},
#endif
      {"wmaker",		no_argument,		NULL, 23},
      {"kde",			no_argument,		NULL, 24},
      {"nokde",			no_argument,		NULL, 25},
      {"nowmaker",		no_argument,		NULL, 26},
      {NULL, 0, NULL, 0}
    };

    int option_index = 0;
    int c = getopt_long_only (argc, argv, "hvV",
			      long_options, &option_index);
    if (c == -1) break;
    switch (c)
      {
      case 0:
	printf ("option %s", long_options[option_index].name);
	if (optarg) printf (" with arg %s", optarg);
	printf ("\n");
	break;
      case 'h': usage(); exit(0); break;
      case 'v': version(); exit(0); break;
      case 'V': Options |= VERBOSE; break;
      case 1:
	optList[NAME].isSet = True;
	optList[NAME].value = optarg;
	break;
      case 2:
	optList[BACKGROUND].isSet = True;
	optList[BACKGROUND].value = optarg;
	break;
      case 3:
	optList[FOREGROUND].isSet = True;
	optList[FOREGROUND].value = optarg;
	break;
      case 4:
	optList[HIGHLIGHT].isSet = True;
	if (optarg) optList[HIGHLIGHT].value = optarg;
	break;
      case 5:
	optList[UPDATE].isSet = True;
	optList[UPDATE].value = optarg;
	break;
      case 6: optList[BELL].isSet = True; break;
      case 7:
	optList[LED].isSet = True;
	if(optarg) optList[LED].value = optarg;
	break;
      case 8:
	optList[FONT].isSet = True;
	optList[FONT].value = optarg;
	break;
      case 9: DisplayName = optarg; break;
      case 10:
	optList[GEOMETRY].isSet = True;
	optList[GEOMETRY].value = optarg;
	break;
      case 11: optList[SHAPE].isSet = True; break;
      case 12:
	optList[SPOOL].isSet = True;
	optList[SPOOL].value = optarg;
	break;
      case 13:
	optList[MAILCOMMAND].isSet = True;
	optList[MAILCOMMAND].value = optarg;
	break;
      case 14:
	optList[CLICKCOMMAND].isSet = True;
	optList[CLICKCOMMAND].value = optarg;
	break;
      case 15: Count_offset = atoi(optarg); break;

#ifdef HAVE_POP3
      case 16:
	optList[POP3].isSet = True;
	optList[SPOOL].isSet = True;
	optList[SPOOL].value = optarg;
	break;
      case 17:
	optList[APOP3].isSet = True;
	optList[SPOOL].isSet = True;
	optList[SPOOL].value = optarg;
	break;
      case 18:
	optList[IMAP].isSet = True;
	optList[SPOOL].isSet = True;
	optList[SPOOL].value = optarg;
	break;
      case 19:
	optList[FOLDERNAME].isSet = True;
	optList[FOLDERNAME].value = optarg;
	break;
      case 20:
	optList[USERNAME].isSet = True;
	optList[USERNAME].value = optarg;
	break;
      case 21:
	optList[PASSWORD].isSet = True;
	optList[PASSWORD].value = strdup(optarg);
	/* Overwrite password argument (for 'ps' scanning) */
	memset(optarg, 0, strlen(optarg));
	break;
      case 22: optList[OFFLINE].isSet = True; break;
#endif
      case 23: optList[WMAKER].isSet = True; break;
      case 24:
	optList[KDE].isSet = True;
	Options |= KDE2_MODE;
	break;
      case 25: optList[NOKDE].isSet = True; break;
      case 26: optList[NOWMAKER].isSet = True; break;
      }
  }

  if(!strcmp(optList[PASSWORD].value, "ask"))
    ask_password();
}

void init(int argc,char *argv[])
{
  int screen;
  XWMHints *wmh;
  XSizeHints *xsh;
  XClassHint *classh;
  XColor color,tmp;
  Window icon=0;
  int x,y,g;

  /* Get any command-line options, these trump the .Xdefaults */
  parse_cmdline(argc, argv);

  dpy=XOpenDisplay(DisplayName);
  if(dpy==NULL)  {
    fprintf(stderr,"error: Can't open display: %s\n",DisplayName);
    exit(1);
  }
  screen=DefaultScreen(dpy);
  DeleteWindow = XInternAtom(dpy, "WM_DELETE_WINDOW", False);

  /* Get the .Xdefault resources and populate our settings */
  parse_xdefaults(dpy, optList[NAME].value);

  if( !(optList[NOKDE].isSet) ) {
    // Look for KDE1 stuff
    KWMDockWindow = XInternAtom(dpy, "KWM_DOCKWINDOW", True);
    if (KWMDockWindow != None)  {
      verbose("found KDE1 stuff, switching on KDE1 mode");
      optList[KDE].isSet = True;
    }
  }

  if( !(optList[NOKDE].isSet) ) {
    // Look for KDE2 "NET" method
    KWMDockWindow = XInternAtom(dpy, "_KDE_NET_WM_SYSTEM_TRAY_WINDOW_FOR",
				True);
    if (KWMDockWindow != None)  {
      verbose("found KDE2 stuff, switching on KDE2 mode");
      Options |= KDE2_MODE;
    }
  }

  if( !(optList[NOWMAKER].isSet) ) {
    // Look for a window maker atom
    Atom a = XInternAtom(dpy, "_WINDOWMAKER_WM_PROTOCOLS", True);
    if (a != None) {
      verbose("found WMaker atom, switching on WM mode");
      optList[WMAKER].isSet = True;
      optList[SHAPE].isSet = True;
    }
  }

  xsh=XAllocSizeHints();
  wmh=XAllocWMHints();
  classh=XAllocClassHint();

  // This font finding stuff is rather icky.
  // It would be better to find a font *after* deciding
  // which mode will actually work.  Font finding should
  // be in a subroutine (!), so it could be re-invoked
  // when a mode switch is needed.

  if (optList[FONT].isSet) {
    // User specified font
    font = XLoadQueryFont(dpy, optList[FONT].value);
    if (!font) {
      fprintf(stderr, "warning: user-specified font not available: %s\n",
	      optList[FONT].value);
    }
  }

  if (!font) {
    // Need to find a font

    if (optList[WMAKER].isSet)
      y=2; // WMaker
    else if ((optList[KDE].isSet) || (Options & KDE2_MODE))
      y=1; // KDE
    else
      y=0; // not docked

    verbose("font list %d", y);

    for (x=0; !font; x++) {
      optList[FONT].value = BackupFonts[y][x];
      if ( !optList[FONT].value ) {
	fprintf(stderr, "error: no available font\n");
	exit(1);
      }
      verbose("considering font %s", optList[FONT].value);
      font = XLoadQueryFont(dpy, optList[FONT].value);
    }
    verbose("found good font");
  }

  font_height();
  Height=Ascent+Descent;
  Width=XTextWidth(font,"88",2);
  xsh->flags = PSize; xsh->width = Width; xsh->height = Height;
  verbose("display region: %d x %d", Width, Height);

  g = XWMGeometry(dpy,screen,optList[GEOMETRY].value,NULL,0,xsh,
		  &x,&y,&Width,&Height,&g);
  if(g&XValue)  { xsh->x = x; xsh->flags |= USPosition; };
  if(g&YValue)  { xsh->y = y; xsh->flags |= USPosition; };
  if(g&WidthValue)  {xsh->width = Width; xsh->flags |= USSize;
  } else            {Width = xsh->width; };
  if(g&HeightValue)  {xsh->height = Height; xsh->flags |= USSize;
  } else             {Height = xsh->height; };

  /*	printf("%dx%d+%d+%d\n",Width,Height,x,y);
	printf("%s%s%s%s\n",
	(xsh->flags&USPosition)?"USPosition ":"",
	(xsh->flags&PPosition)?"PPosition ":"",
	(xsh->flags&USSize)?"USSize ":"",
	(xsh->flags&PSize)?"PSize ":"");
  */

  win=XCreateSimpleWindow(dpy,RootWindow(dpy,screen),x,y,Width,Height,0,
			  BlackPixel(dpy,screen),WhitePixel(dpy,screen));

  wmh->initial_state = NormalState;
  wmh->input = False;
  wmh->window_group = win;
  wmh->flags = StateHint | WindowGroupHint;
  classh->res_name = optList[NAME].value;
  classh->res_class = APL_CLASS;

  if(optList[WMAKER].isSet) {
    verbose("using WMaker mode");
    icon=XCreateSimpleWindow(dpy,RootWindow(dpy,screen),x,y,Width,Height,
			     0,BlackPixel(dpy,screen),WhitePixel(dpy,screen));
    wmh->initial_state=WithdrawnState;
    wmh->icon_window=icon;
    wmh->flags |= IconWindowHint;
    XSetClassHint(dpy, icon, classh);
    XSelectInput(dpy, icon,
		 ExposureMask
		 | ButtonPressMask
		 | KeyPressMask
		 | StructureNotifyMask);
  }

  XmbSetWMProperties(dpy, win, "xlassie", "xlassie", argv, argc,
		     xsh, wmh, classh);
  XSetWMProtocols(dpy, win, &DeleteWindow, 1);
  XSelectInput(dpy, win,
	       ExposureMask
	       | ButtonPressMask
	       | KeyPressMask
	       | StructureNotifyMask);
  XFlush(dpy);

  if(optList[KDE].isSet) {
    unsigned char data = 1;
    verbose("using KDE1 mode");
    if (KWMDockWindow == None) {
      verbose("KDE1 mode isn't going to work, skipping");
    } else {
      XChangeProperty(dpy, win, KWMDockWindow, KWMDockWindow, 8,
		      PropModeReplace, &data, 1);
    }
  }

  if (Options & KDE2_MODE) {
    verbose("using KDE2 mode");
    if (KWMDockWindow == None) {
      verbose("KDE2 mode isn't going to work, skipping");
    } else {
      XChangeProperty(dpy, win, KWMDockWindow, XA_WINDOW, 32,
		      PropModeReplace, (void*)&win, 1);
    }
  }

  XMapWindow(dpy, win);

  if(optList[WMAKER].isSet) win = icon;	// Use the icon window from now on

  gc = XCreateGC(dpy,win,0,NULL);
  XAllocNamedColor(dpy,DefaultColormap(dpy,screen),
		   optList[FOREGROUND].value,&color,&tmp);
  XSetForeground(dpy,gc,color.pixel); FgXColor=color;
  XAllocNamedColor(dpy,DefaultColormap(dpy,screen),
		   optList[BACKGROUND].value,&color,&tmp);
  XSetBackground(dpy,gc,color.pixel);
  XSetWindowBackground(dpy,win,color.pixel);
  if(optList[HIGHLIGHT].isSet)
    XAllocNamedColor(dpy,DefaultColormap(dpy,screen),
		     optList[HIGHLIGHT].value,&HiXColor,&tmp);
  XSetFont(dpy,gc,font->fid);

  if(optList[SHAPE].isSet) {
    verbose("using shaped window");
    MaskWidth = Width; MaskHeight = Height;
    ShapeMask = XCreatePixmap(dpy,win,MaskWidth,MaskHeight,1);
    ShapeGC = XCreateGC(dpy,ShapeMask,0,NULL);
    XSetFont(dpy,ShapeGC,font->fid);
  };
}

int check_spool()
{
  struct stat st;
  char str[PATH_MAX];

  if(optList[POP3].isSet || optList[APOP3].isSet || optList[IMAP].isSet)
    return 0;

  if(stat(optList[SPOOL].value, &st)) {
    perror("stat");
    fprintf(stderr, "error: mail file/directory not found '%s'\n",
	    optList[SPOOL].value);
    exit(1);
  }

  if(S_ISDIR(st.st_mode)) {
    Options|=USE_MAILDIR;

    /* Check for the subdir "new" and use it if it exists */
    strcpy(str, optList[SPOOL].value);
    strcat(str, (str[strlen(str)-1]=='/')?"new":"/new");
    if(!stat(str, &st) && S_ISDIR(st.st_mode)) {
      strcpy(optList[SPOOL].value, str);
    }
  }
  return 1;
}

int main(int argc,char *argv[])
{
  XEvent xev;
  Atom WM_PROTOCOLS;
  struct itimerval itv;
  struct sigaction sig;
  struct passwd* pwd;
  char *mail;

  pwd = getpwuid(getuid());
  mail= getenv("MAIL");
  if(mail==NULL) {
    asprintf(&optList[SPOOL].value, "/var/spool/mail/%s",
	    pwd->pw_name);
  } else {
    optList[SPOOL].value = mail;
  }

  /* Set some defaults requiring external struct accesses */
  optList[USERNAME].value = pwd->pw_name;

  init(argc,argv);
  check_spool();

  WM_PROTOCOLS = XInternAtom(dpy, "WM_PROTOCOLS", False);

#ifdef HAVE_POP3
# ifdef OFFLINE_DEFAULT
  Options|=OFFLINE;
# endif
#endif

  count_mail();

  if(optList[BELL].isSet && Number) XBell(dpy,100);

  sig.sa_handler=handler;
  sigemptyset(&sig.sa_mask);
  sig.sa_flags=SA_RESTART;
  sigaction(SIGALRM,&sig,NULL);
  sigaction(SIGINT,&sig,NULL);
  sigaction(SIGTERM,&sig,NULL);
#ifdef HAVE_POP3
  if ((optList[POP3].isSet  ||
       optList[APOP3].isSet ||
       optList[IMAP].isSet) && (!(optList[UPDATE].isSet)))
    itv.it_interval.tv_sec = atoi(INTERVAL_REMOTE);
  else
#endif
    itv.it_interval.tv_sec = atoi(optList[UPDATE].value);
  itv.it_interval.tv_usec = 0;
  itv.it_value = itv.it_interval;
  setitimer(ITIMER_REAL,&itv,NULL);

  for(;;)  {
    XNextEvent(dpy,&xev);
    switch(xev.type)  {
    case Expose:
      while(XCheckTypedEvent(dpy,Expose,&xev));
      update();
      break;
    case ButtonPress:
      verbose("button click!");
      if (strcmp(optList[CLICKCOMMAND].value,
		 "CHECK_MAILBOX") != 0) {
	verbose("calling click command");
	system(optList[CLICKCOMMAND].value);
      } else {
	verbose("raising SIGALRM");
	raise(SIGALRM);
      }
      break;
    case KeyPress:
      if (XLookupKeysym(&xev.xkey, 0) == XK_q) {
	verbose("quit: keypress q");
	close_display();
	exit(0);
      }
      break;
    case ConfigureNotify:
      Width = xev.xconfigure.width;
      Height = xev.xconfigure.height;
      update();
      break;
    case ClientMessage:
      if(xev.xclient.message_type == WM_PROTOCOLS)  {
	if(xev.xclient.data.l[0] == DeleteWindow) {
	  verbose("quit: DeleteWindow");
	  close_display();
	  exit(0);
	};
      };
      break;
    case DestroyNotify:
      verbose("quit: DestroyNotify");
      close_display();
      exit(0);
    default:
      verbose("warning: unknown xev.type %d", xev.type);
    };
  };
}

#ifdef HAVE_POP3
int sock_connect(char *,int);

FILE *pop_login()
{
  int fd;
  FILE *f;
  char buf[256];

  fd=sock_connect(optList[SPOOL].value, 110);
  if(fd==-1)  {
    if(optList[OFFLINE].isSet) {
      Number=-1; return NULL;
    } else {
      fprintf(stderr, "error: cannot connect to POP3 server %s\n",
	      optList[SPOOL].value);
      exit(1);
    };
  };

  f=fdopen(fd,"r+");

  fgets(buf,256,f);

  fflush(f); fprintf(f,"USER %s\r\n", optList[USERNAME].value);
  fflush(f); fgets(buf,256,f);

  if(buf[0]!='+')  {
    fprintf(stderr,"warning: server rejected username\n");
    fprintf(f,"QUIT\r\n");fclose(f);
    return NULL;
  };
  fflush(f); fprintf(f,"PASS %s\r\n", optList[PASSWORD].value);
  fflush(f); fgets(buf,256,f);
  if(buf[0]!='+')  {
    fprintf(stderr,"warning: server rejected password\n");
    fprintf(stderr," server resposne: %s",buf);
    fprintf(f,"QUIT\r\n");fclose(f);
    return NULL;
  };

  return(f);
}

FILE *imap_login()
{
  int fd;
  FILE *f;
  char buf[128];

  fd=sock_connect(optList[SPOOL].value, 143);
  if(fd==-1)  {
    if(optList[OFFLINE].isSet) {
      Number=-1; return NULL;
    } else {
      fprintf(stderr, "error: cannot connect to IMAP server %s\n",
	      optList[SPOOL].value);
      exit(1);
    };
  };

  f=fdopen(fd, "r+");

  fgets(buf, 127, f);
  fflush(f); fprintf(f, "a001 LOGIN %s \"%s\"\r\n",
		     optList[USERNAME].value, optList[PASSWORD].value);
  fflush(f); fgets(buf, 127, f);

  if(buf[5]!='O')  {       /* Looking for "a001 OK" */
    fprintf(stderr, "warning: server rejected name and/or password.\n");
    fprintf(f, "a002 LOGOUT\r\n"); fclose(f);
    return NULL;
  };

  return f;
}

int count_mail_pop()
{
  FILE *f;
  char buf[256];
  int total,read;

  f = pop_login();
  if(f==NULL) return -1;

  fflush(f); fprintf(f,"STAT\r\n");
  fflush(f); fgets(buf,256,f);
  sscanf(buf,"+OK %d",&total);
  fflush(f); fprintf(f,"LAST\r\n");
  fflush(f); fgets(buf,256,f);
  sscanf(buf,"+OK %d",&read);
  Number=total-read;

  fprintf(f,"QUIT\r\n");
  fclose(f);

  return 1;
}

struct msg {
  int size;
  int read;
};

struct msg *get_list(FILE *f,int *nout)
{
  char buf[256];
  struct msg *l;
  int i,n;

  fflush(f); fprintf(f,"STAT\r\n");
  fflush(f); fgets(buf,256,f);
  sscanf(buf,"+OK %d",&n);

  l=malloc(sizeof(struct msg)*n);
  fflush(f); fprintf(f,"LIST\r\n");
  fflush(f); fgets(buf,256,f);
  if(strncmp(buf,"+OK",3))  {
    fprintf(stderr, "warning: can't get list.\n");
    return NULL;
  };

  for(i=0;i<n;i++)  {
    fgets(buf,256,f);
    sscanf(buf,"%*d %d",&l[i].size);
  };
  fgets(buf,256,f);
  if(buf[0]!='.')  {
    fprintf(stderr,"warning: too many messages.\n");
    return NULL;
  };

  *nout=n;
  return l;
}

void get_header(FILE *f,struct msg *l,int i)
{
  char buf[256];

  fflush(f); fprintf(f,"TOP %d 1\r\n",i+1);
  fflush(f); fgets(buf,256,f);
  if(strncmp(buf,"+OK",3))  {
    fprintf(stderr, "warning: TOP failed.\n");
    fprintf(stderr,"   server returned: %s\n",buf);
    return;
  };
  l->read=0;
  for(;;)  {
    fgets(buf,256,f);
    if(!strcmp(buf,".\r\n"))  break;
    if(!strncmp(buf,"Status: R",9))
      l->read=1;
  };
  /* fprintf(stderr,"msg %d %s\n",i+1,l->read?"read":"unread"); */
}

int count_mail_pop_2()
{
  FILE *f;
  int i,j,n2;
  int total=0;
  static struct msg *l1=NULL,*l2;
  static int n;

  f = pop_login();
  if(f==NULL) return -1;

  if(l1==NULL)  {  /* get initial list */
    l1=get_list(f,&n);
    if(l1==NULL) return -1;
    for(i=0;i<n;i++) {
      get_header(f,l1+i,i);
      if(!l1[i].read) total++;
    };
    Number=total;
    return 1;
  };

  l2=get_list(f,&n2);

  /* compare lists, retreive new messages */
  for(i=0,j=0;i<n && j<n2;i++,j++)  {
    /*		fprintf(stderr,"Old list %d size %d %c, new list %d size %d\n",
		i+1,l1[i].size,l1[i].read?'R':'U', j+1,l2[j].size);
    */
    if(l1[i].size != l2[j].size) break;
    if(!l1[i].read)  {
      get_header(f,l2+j,j);
    } else {
      l2[j].read=1;
    };
  };
  for(;j<n2;j++) get_header(f,l2+j,j);

  fclose(f);

  free(l1); l1=l2; l2=NULL; n=n2;
  for(i=0;i<n;i++)  if(!l1[i].read) total++;
  Number=total;

  return 1;
}

int count_mail_imap()
{
  FILE *f;
  char srch1[128];
  char srch2[128];
  char buf[128];
  int total=0;

  f = imap_login();
  if(f==NULL) return -1;

  verbose("To server: a003 STATUS %s (UNSEEN)\r\n", optList[FOLDERNAME].value);
  fflush(f); fprintf(f, "a003 STATUS %s (UNSEEN)\r\n", optList[FOLDERNAME].value);
  fflush(f); fgets(buf, 127, f);
  sprintf(srch1,"* STATUS %s (UNSEEN %%d)", optList[FOLDERNAME].value);
  sprintf(srch2,"* STATUS \"%s\" (UNSEEN %%d)", optList[FOLDERNAME].value);
  if(!sscanf(buf, srch1, &total) &&
     !sscanf(buf, srch2, &total)) {
    fprintf(stderr, "warning: unexpected response from server\n");
    fprintf(stderr, " while searching either: %s\n", srch1);
    fprintf(stderr, " or: %s\n", srch2);
    fprintf(stderr, " response was: %s", buf);
    return 0;
  }

  Number = total;

  fclose(f);

  return 1;
}
#endif

int count_mail_mbox()
{
  static time_t mtime=0;
  static off_t size=0;
  struct stat st;
  int isread=0;
  FILE *spool;
  char buf[256];

  spool = fopen(optList[SPOOL].value, "r");
  if(spool==NULL)  {
    perror("xlassie");
    fprintf(stderr,"error: failed to open spoolfile '%s'\n",
	    optList[SPOOL].value);
    close_display();
    exit(errno);
  };

  if(fstat(fileno(spool),&st)) {
    perror("fstat");
    fprintf(stderr,"error: failed to stat spoolfile '%s'\n",
	    optList[SPOOL].value);
    close_display();
    exit(errno);
  };
  /* check to see if file was changed */
  if(st.st_mtime != mtime || st.st_size != size)  {
    mtime = st.st_mtime;
    size = st.st_size;
    Number = 0;
    while(fgets(buf, 256, spool)) {
      if(!strncmp(buf, "From ", 5))  {
	Number++; isread=0;
      } else if(!strncmp(buf, "Status: R", 9))  {
	if(!isread) { Number--; isread=1; };
      };
    };
  }
  fclose(spool);

  return 1;
}

// In the distant future xlassie could be modified to use the Linux
// system call for requesting notification when a directory has an
// entry added or removed or modified.

// Or, could just use scandir(3) instead of doing this all manually.

int count_mail_maildir()
{
  static time_t mtime=0;
  struct stat st;
  DIR *dir;
  struct dirent *de;

  if(stat(optList[SPOOL].value, &st)) {
    perror("stat");
    fprintf(stderr, "error: unable to access mail directory '%s'\n",
	    optList[SPOOL].value);
    close_display();
    exit(errno);
  }

  if(st.st_mtime != mtime) {
    mtime = st.st_mtime;
    if ( !(dir = opendir(optList[SPOOL].value)) ) {
      perror("opendir");
      fprintf(stderr, "error: unable to access mail directory '%s'\n",
	      optList[SPOOL].value);
      close_display();
      exit(errno);
    }
    Number = 0;
    while( (de = readdir(dir)) != NULL ) {
      if (de->d_name[0] != '.') Number++;
    }
    closedir(dir);
  }
  return 1;
}

int count_mail()
{
  verbose("about to count mail");

  if(Options&USE_MAILDIR) {
    return count_mail_maildir();
#ifdef HAVE_POP3
  } else if(optList[APOP3].isSet)  {
    return count_mail_pop_2();
  } else if(optList[POP3].isSet)  {
    return count_mail_pop();
  } else if(optList[IMAP].isSet) {
    return count_mail_imap();
#endif
  } else {
    return count_mail_mbox();
  }
}
